#!/bin/bash

# Grab our input
urlpath="$1"

# Before we can proceed, is this a blacklisted repository?
# We blacklist them as these repositories are used by all builds, and Jenkins is incapable of ignoring a repository for polling (bug in Jenkins)
if [[ "$urlpath" = "sysadmin/ci-tooling" ]] || [[ "$urlpath" = "sysadmin/repo-metadata" ]] || [[ "$urlpath" = "frameworks/kapidox" ]] || [[ "$urlpath" = "sdk/kde-dev-scripts" ]]; then
    exit 0
fi

# Tell the KDE CI system
curl --connect-timeout 2 --max-time 2 "https://build.kde.org/git/notifyCommit?url=https://invent.kde.org/$urlpath" &> /dev/null
# Also tell the Binary Factory
curl --connect-timeout 2 --max-time 2 "https://binary-factory.kde.org/git/notifyCommit?url=https://invent.kde.org/$urlpath" &> /dev/null
